namespace java org.jitsi.jicofo.learning
namespace py org.jitsi.jicofo.learning

struct Metrics {
	1:string name,
	2:list<string> metadata,
	3:list<double> parameters
}

struct TrainedModel {
	1:string name,
	2:list<Metrics> metrics
}

service BridgeLearner {
	TrainedModel train(1:string jvb)
}