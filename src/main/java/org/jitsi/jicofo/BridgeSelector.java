/*
 * Jicofo, the Jitsi Conference Focus.
 *
 * Copyright @ 2015 Atlassian Pty Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jitsi.jicofo;

import net.java.sip.communicator.impl.protocol.jabber.extensions.colibri.ColibriStatsExtension;
import net.java.sip.communicator.util.Logger;
import org.apache.thrift.TException;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.jitsi.assertions.Assert;
import org.jitsi.eventadmin.EventAdmin;
import org.jitsi.jicofo.aws.EC2Utils;
import org.jitsi.jicofo.discovery.Version;
import org.jitsi.jicofo.event.BridgeEvent;
import org.jitsi.jicofo.scheduling.thrift.Scheduler;
import org.jitsi.protocol.xmpp.OperationSetSubscription;
import org.jitsi.protocol.xmpp.SubscriptionListener;
import org.jitsi.service.configuration.ConfigurationService;
import org.jitsi.util.StringUtils;
import org.jivesoftware.smack.packet.PacketExtension;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class exposes methods for selecting best videobridge from all currently
 * available. Videobridge state is tracked through PubSub notifications and
 * based on feedback from Jitsi Meet conference focus.
 *
 * @author Pawel Domas
 */
public class BridgeSelector
    implements SubscriptionListener, Scheduler.Iface, Runnable
{
    /**
     * The logger.
     */
    private final static Logger logger = Logger.getLogger(BridgeSelector.class);

    private static final int THRIFT_SERVER_PORT = 10090;

    /**
     * Property used to configure mapping of videobridge JIDs to PubSub nodes.
     * Single mapping is defined by writing videobridge JID followed by ':' and
     * pub-sub node name. If multiple mapping are to be appended then ';' must
     * be used to separate each mapping.
     *
     * org.jitsi.focus.BRIDGE_PUBSUB_MAPPING
     * =jvb1.server.net:pubsub1;jvb2.server.net:pubsub2;jvb3.server.net:pubsub3
     *
     * PubSub service node is discovered automatically for now and the first one
     * that offer PubSub feature is selected. Then this selector class
     * subscribes for all mapped PubSub nodes on that service for notifications.
     *
     * FIXME: we do not unsubscribe from pubsub notifications on shutdown
     */
    public static final String BRIDGE_TO_PUBSUB_PNAME
        = "org.jitsi.focus.BRIDGE_PUBSUB_MAPPING";

    /**
     * Configuration property which specifies the amount of time since bridge
     * instance failure before the selector will give it another try.
     */
    public static final String BRIDGE_FAILURE_RESET_THRESHOLD_PNAME
        = "org.jitsi.focus.BRIDGE_FAILURE_RESET_THRESHOLD";

    /**
     * Five minutes.
     */
    public static final long DEFAULT_FAILURE_RESET_THRESHOLD = 5L * 60L * 1000L;

    /**
     * Environment variable to enable scale under EC2 environment
     */
    public static final String ENABLE_SCALE = "ENABLE_SCALE";

    /**
     * Environment variable to enable thresholds
     */
    public static final String ENABLE_THRESHOLDS = "ENABLE_THRESHOLDS";

    /**
     * The count of spawned EC2 nodes
     */
    private AtomicInteger ec2Count = new AtomicInteger(0);

    /**
     * Default limit on EC2 nodes
     */
    public static final int DEFAULT_EC2_LIMIT = 40;

    /**
     * Environment variable string for ec2 limit
     */
    public static final String EC2_LIMIT = "EC2_LIMIT";

    /**
     * Default JVB AMI
     */
    public static final String DEFAULT_JVB_AMI = "ami-ffa5739f";

    /**
     * Environment variable string for jvb ami
     */
    public static final String JVB_AMI = "JVB_AMI";

    /**
     * Scaling factor like 15% before the preset values happen
     */
    public static final String SCALING_FACTOR = "SCALING_FACTOR";

    /**
     * Default scaling factor
     */
    public static final double DEFAULT_SCALING_FACTOR = 0.85;

    /**
     * Environment variable string for EC2 instance type
     */
    public static final String EC2_INSTANCE_TYPE = "EC2_INSTANCE_TYPE";

    /**
     * Default EC2 instance type
     */
    public static final String DEFAULT_EC2_INSTANCE_TYPE = "t2.micro";

    /**
     * The amount of time we will wait after bridge instance failure before it
     * will get another chance.
     */
    private long failureResetThreshold = DEFAULT_FAILURE_RESET_THRESHOLD;

    /**
     * Operation set used to subscribe to PubSub nodes notifications.
     */
    private OperationSetSubscription subscriptionOpSet;


    /**
     * The map of bridge JID to <tt>BridgeState</tt>.
     */
    private final Map<String, BridgeState> bridges = new ConcurrentHashMap<>();

    /**
     * The <tt>EventAdmin</tt> used by this instance to fire/send
     * <tt>BridgeEvent</tt>s.
     */
    private EventAdmin eventAdmin;

    /**
     * Pre-configured JVB used as last chance option even if no bridge has been
     * auto-detected on startup.
     */
    private String preConfiguredBridge;

    /**
     * The map of Pub-Sub nodes to videobridge JIDs.
     */
    private final Map<String, String> pubSubToBridge = new HashMap<>();

    /**
     *
     */
    private long lastEC2Time = System.currentTimeMillis();

    /**
     * Default EC2 gap
     */
    private static final double DEFAULT_EC2_GAP = 1.5;

    /**
     * Environment variable String for EC2 gap
     */
    private static final String EC2_GAP = "EC2_GAP";

    /**
     * List for connected JVBs
     */
    private List<Integer> jvbList = new LinkedList<>();

    /**
     * Creates new instance of {@link BridgeSelector}.
     *
     * @param subscriptionOpSet the operations set that will be used by this
     *                          instance to subscribe to pub-sub notifications.
     */
    public BridgeSelector(OperationSetSubscription subscriptionOpSet)
    {
        Assert.notNull(subscriptionOpSet, "subscriptionOpSet");

        this.subscriptionOpSet = subscriptionOpSet;
    }

    public BridgeSelector()
    {

    }

    /**
     * Adds next Jitsi Videobridge XMPP address to be observed by this selected
     * and taken into account in best bridge selection process.
     *
     * @param bridgeJid the JID of videobridge to be added to this selector's
     *                  set of videobridges.
     */
    public void addJvbAddress(String bridgeJid)
    {
        addJvbAddress(bridgeJid, null);
    }

    /**
     * Adds next Jitsi Videobridge XMPP address to be observed by this selected
     * and taken into account in best bridge selection process.
     *
     * @param bridgeJid the JID of videobridge to be added to this selector's
     *                  set of videobridges.
     * @param version the {@link Version} IQ instance which contains the info
     *                about JVB version.
     */
    public void addJvbAddress(String bridgeJid, Version version)
    {
        if (isJvbOnTheList(bridgeJid))
        {
            return;
        }

        logger.info("Added videobridge: " + bridgeJid + " v: " + version);

        BridgeState newBridge = new BridgeState(bridgeJid, version);

        bridges.put(bridgeJid, newBridge);

        notifyBridgeUp(newBridge);

        logger.info("cpu:" + newBridge.updateAndGetCPUUsage() + " mem:" + newBridge.updateAndGetMemoryUsage());
        logger.info("bridge size:" + bridges.size());
    }

    /**
     * Returns <tt>true</tt> if given JVB XMPP address is already known to this
     * <tt>BridgeSelector</tt>.
     *
     * @param jvbJid the JVB JID to be checked eg. jitsi-videobridge.example.com
     *
     * @return <tt>true</tt> if given JVB XMPP address is already known to this
     * <tt>BridgeSelector</tt>.
     */
    boolean isJvbOnTheList(String jvbJid)
    {
        return bridges.containsKey(jvbJid);
    }

    /**
     * Removes Jitsi Videobridge XMPP address from the list videobridge
     * instances available in the system .
     *
     * @param bridgeJid the JID of videobridge to be removed from this selector's
     *                  set of videobridges.
     */
    public void removeJvbAddress(String bridgeJid)
    {
        logger.info("Removing JVB: " + bridgeJid);

        BridgeState bridge = bridges.remove(bridgeJid);

        if (bridge != null)
            notifyBridgeDown(bridge);

        logger.info("bridge size:" + bridges.size());
    }

    /**
     * Returns least loaded and *operational* videobridge. By operational it
     * means that it was not reported by any of conference focuses to fail while
     * allocating channels.
     *
     * @return the JID of least loaded videobridge or <tt>null</tt> if there are
     *         no operational bridges currently available.
     */
    public String selectVideobridge(String roomName, int conferenceSize)
    {
        // check the suffix of roomname
        // if it has _xx_T, it is for training
        List<BridgeState> bridges = getPrioritizedBridgesList(conferenceSize);
        if (bridges.size() == 0)
            return null;

        int at = roomName.indexOf("@");
        if (roomName.charAt(at-1) == 't') {
            String jvbNum = roomName.substring(at-4, at-2);
            for (BridgeState b : bridges) {
                if (b.jid.contains(jvbNum)) {
                    logger.info("Found " + b.jid + " for training.");
                    return b.isOperational ? b.jid : null;
                }
            }
            return null;
        } else {
            // TODO use predictor to pick those that will be under thresholds after the new conference
            logger.debug("selectBridge with size:" + conferenceSize + " on:" + roomName);

            return bridges.get(0).isOperational() ? bridges.get(0).jid : null;
        }
    }

    /**
     * Deprecated
     * @return
     */
    public String selectVideobridge() {
        List<BridgeState> bridges = getPrioritizedBridgesList();
        if (bridges.size() == 0)
            return null;
        return bridges.get(0).isOperational() ? bridges.get(0).jid : null;
    }

    /**
     * Deprecated
     * @return
     */
    private List<BridgeState> getPrioritizedBridgesList() {
        ArrayList<BridgeState> bridgeList = new ArrayList<>(bridges.values());

        Collections.sort(bridgeList);

        Iterator<BridgeState> bridgesIter = bridgeList.iterator();

        while (bridgesIter.hasNext()) {
            BridgeState bridge = bridgesIter.next();
            if (!bridge.isOperational() || bridge.exceedsThresholds(0))
                bridgesIter.remove();
        }
        return bridgeList;
    }


    /**
     * Returns the list of all known videobridges JIDs ordered by load and
     * *operational* status. Not operational bridges are at the end of the list.
     */
    private List<BridgeState> getPrioritizedBridgesList(int conferenceSize)
    {
        ArrayList<BridgeState> bridgeList = new ArrayList<>(bridges.values());

        Collections.sort(bridgeList);

        Iterator<BridgeState> bridgesIter = bridgeList.iterator();

        while (bridgesIter.hasNext())
        {
            BridgeState bridge = bridgesIter.next();

            logger.info("ENABLE_THRESHOLDS:" + System.getenv(ENABLE_THRESHOLDS));
            if (System.getenv(ENABLE_THRESHOLDS).equals("true")) {
                logger.info("Enabled thresholds.");
                if (!bridge.isOperational() || bridge.exceedsThresholds(conferenceSize)) {
                    logger.info("Remove jvb due to thresholds.");
                    bridgesIter.remove();
                    logger.info("Bridge list size:" + bridgeList.size());
                }
            } else {
                if (!bridge.isOperational()) {
                    logger.info("Remove jvb due to non-operational status.");
                    bridgesIter.remove();
                    logger.info("Bridge list size:" + bridgeList.size());
                }
            }
        }

        if (System.getenv(ENABLE_SCALE).equals("true")) {
            // when there is no available nodes
            // we start up new ec2 instances
            if (bridgeList.size() == 0) {

                long timeDiff = System.currentTimeMillis() - (long)(lastEC2Time + getEc2Gap() * 1000 * 60);
                if (timeDiff > 0) {
                    logger.info("Time diff:" + timeDiff + " lastEC2Time:" + lastEC2Time);
                    logger.info("Running out of available jvbs, starting new EC2 nodes now.");
                    lastEC2Time = System.currentTimeMillis();
                    FocusBundleActivator.getSharedThreadPool().execute(
                            new Runnable() {
                                @Override
                                public void run() {
                                    logger.info("EC2 runInstanceResult");
                                    if (ec2Count.get() < getEc2Limit()) {
                                        EC2Utils.startInstance(getJvbAmi(), "thesis", "sg-2171e746", getEc2InstanceType());
                                        ec2Count.addAndGet(1);
                                        logger.info("Scale another JVB, now:" + ec2Count.toString() + " nodes.");
                                    } else {
                                        logger.info("Reaching the scaling limit: " + getEc2Limit() + ".");
                                    }
                                }
                            }
                    );
                } else {
                    logger.info("Time diff:" + timeDiff + " lastEC2Time:" + lastEC2Time);
                    logger.info("Just spawned a node less than " + getEc2Gap() + " mins before. Wait a little.");
                }

            }
        }
        return bridgeList;
    }

    /**
     * Getter for JVB AMI
     * @return jvb ami
     */
    private String getJvbAmi() {
        String ami = System.getenv(JVB_AMI);
        if (ami == null) {
            return DEFAULT_JVB_AMI;
        } else {
            return ami;
        }
    }

    /**
     * Getter for EC2 limit
     * @return ec2 limit
     */
    private int getEc2Limit() {
        String ec2Limit = System.getenv(EC2_LIMIT);
        if (ec2Limit == null) {
            return DEFAULT_EC2_LIMIT;
        } else {
            return Integer.valueOf(ec2Limit);
        }
    }

    /**
     * Getter for scaling factor
     * @return
     */
    private double getScalingFactor() {
        String scalingFactor = System.getenv(SCALING_FACTOR);
        if (scalingFactor == null) {
            return DEFAULT_SCALING_FACTOR;
        } else {
            return Double.valueOf(scalingFactor);
        }
    }

    /**
     * Getter for EC2 gap
     * @return
     */
    private double getEc2Gap() {
        String ec2Gap = System.getenv(EC2_GAP);
        if (ec2Gap == null) {
            return DEFAULT_EC2_GAP;
        } else {
            return Double.valueOf(ec2Gap);
        }
    }

    /**
     * Getter for EC2 instance type
     * @return
     */
    private String getEc2InstanceType() {
        String ec2InstanceType = System.getenv(EC2_INSTANCE_TYPE);
        if (ec2InstanceType == null) {
            return DEFAULT_EC2_INSTANCE_TYPE;
        } else {
            return ec2InstanceType;
        }
    }

    /**
     * Updates given *operational* status of the videobridge identified by given
     * <tt>bridgeJid</tt> address.
     *
     * @param bridgeJid the XMPP address of the bridge.
     * @param isWorking <tt>true</tt> if bridge successfully allocated
     *                  the channels which means it is in *operational* state.
     */
    public void updateBridgeOperationalStatus(String bridgeJid,
                                              boolean isWorking)
    {
        BridgeState bridge = bridges.get(bridgeJid);
        if (bridge != null)
        {
            bridge.setIsOperational(isWorking);
        }
        else
        {
            logger.warn("No bridge registered for jid: " + bridgeJid);
        }
    }

    /**
     * Returns videobridge JID for given pub-sub node.
     *
     * @param pubSubNode the pub-sub node name.
     *
     * @return videobridge JID for given pub-sub node or <tt>null</tt> if no
     *         mapping found.
     */
    public String getBridgeForPubSubNode(String pubSubNode)
    {
        BridgeState bridge = findBridgeForNode(pubSubNode);
        return bridge != null ? bridge.jid : null;
    }

    /**
     * Finds <tt>BridgeState</tt> for given pub-sub node.
     *
     * @param pubSubNode the name of pub-sub node to match with the bridge.
     *
     * @return <tt>BridgeState</tt> for given pub-sub node name.
     */
    private BridgeState findBridgeForNode(String pubSubNode)
    {
        String bridgeJid = pubSubToBridge.get(pubSubNode);
        if (bridgeJid != null)
        {
            return bridges.get(bridgeJid);
        }
        return null;
    }

    /**
     * Finds pub-sub node name for given videobridge JID.
     *
     * @param bridgeJid the JID of videobridge to be matched with
     *                  pub-sub node name.
     *
     * @return name of pub-sub node mapped for given videobridge JID.
     */
    private String findNodeForBridge(String bridgeJid)
    {
        for (Map.Entry<String, String> psNodeToBridge
            : pubSubToBridge.entrySet())
        {
            if (psNodeToBridge.getValue().equals(bridgeJid))
            {
                return psNodeToBridge.getKey();
            }
        }
        return null;
    }

    /**
     * Method called by {@link org.jitsi.jicofo.ComponentsDiscovery
     * .ThroughPubSubDiscovery} whenever we receive stats update on shared
     * PubSub node used to discover bridges.
     * @param itemId stats item ID. Should be the JID of JVB instance.
     * @param payload JVB stats payload.
     */
    void onSharedNodeUpdate(String itemId, PacketExtension payload)
    {
        onSubscriptionUpdate(null, itemId, payload);
    }

    /**
     * Pub-sub notification processing logic.
     *
     * {@inheritDoc}
     */
    @Override
    public void onSubscriptionUpdate(String          node,
                                                  String          itemId,
                                                  PacketExtension payload)
    {
        if (!(payload instanceof ColibriStatsExtension))
        {
            logger.error(
                "Unexpected pub-sub notification payload: "
                    + payload.getClass().getName());
            return;
        }

        BridgeState bridgeState = null;
        if (node != null)
        {
            bridgeState = findBridgeForNode(node);
        }

        if (bridgeState == null)
        {
            // Try to figure out bridge by itemId
            bridgeState = bridges.get(itemId);
            if (bridgeState == null)
            {
                logger.warn(
                        "Received PubSub update for unknown bridge: "
                            + itemId + " node: "
                            + (node == null ? "'shared'" : node));
                return;
            }
        }

        ColibriStatsExtension stats = (ColibriStatsExtension) payload;
        for (PacketExtension child : stats.getChildExtensions())
        {
            if (!(child instanceof ColibriStatsExtension.Stat))
            {
                continue;
            }

            ColibriStatsExtension.Stat stat
                = (ColibriStatsExtension.Stat) child;
            if ("conferences".equals(stat.getName()))
            {
                Integer val = getStatisticIntValue(stat);
                if(val != null)
                    bridgeState.setConferenceCount(val);
            }
            else if ("videochannels".equals(stat.getName()))
            {
                Integer val = getStatisticIntValue(stat);
                if(val != null)
                    bridgeState.setVideoChannelCount(val);
            }
            else if ("videostreams".equals(stat.getName()))
            {
                Integer val = getStatisticIntValue(stat);
                if(val != null)
                    bridgeState.setVideoStreamCount(val);
            }
        }
    }

    /**
     * Extracts the statistic integer value from <tt>currentStats</tt> if
     * available and in correct format.
     * @param currentStats the current stats
     */
    private static Integer getStatisticIntValue(
        ColibriStatsExtension.Stat currentStats)
    {
        Object obj = currentStats.getValue();
        if (obj == null)
        {
            return null;
        }
        String str = obj.toString();
        try
        {
            return Integer.valueOf(str);
        }
        catch(NumberFormatException e)
        {
            logger.error("Error parsing stat item: " + currentStats.toXML());
        }
        return null;
    }

    /**
     * Returns the JID of pre-configured Jitsi Videobridge instance.
     */
    public String getPreConfiguredBridge()
    {
        return preConfiguredBridge;
    }

    /**
     * Sets the JID of pre-configured JVB instance which will be used when all
     * auto-detected bridges are down.
     * @param preConfiguredBridge XMPP address of pre-configured JVB component.
     *
     * @throws NullPointerException if <tt>preConfiguredBridge</tt> is
     *         <tt>null</tt>.
     */
    public void setPreConfiguredBridge(String preConfiguredBridge)
    {
        Assert.notNull(preConfiguredBridge, "preConfiguredBridge");

        logger.info("Configuring default bridge: " + preConfiguredBridge);

        addJvbAddress(preConfiguredBridge);

        this.preConfiguredBridge = preConfiguredBridge;
    }

    /**
     * The time since last bridge failure we will wait before it gets another
     * chance.
     *
     * @return failure reset threshold in millis.
     */
    public long getFailureResetThreshold()
    {
        return failureResetThreshold;
    }

    /**
     * Sets the amount of time we will wait after bridge failure before it will
     * get another chance.
     *
     * @param failureResetThreshold the amount of time in millis.
     *
     * @throws IllegalArgumentException if given threshold value is equal or
     *         less than zero.
     */
    public void setFailureResetThreshold(long failureResetThreshold)
    {
        if (failureResetThreshold <= 0)
        {
            throw new IllegalArgumentException(
                "Bridge failure reset threshold" +
                    " must be greater than 0, given value: " +
                    failureResetThreshold);
        }
        this.failureResetThreshold = failureResetThreshold;
    }

    /**
     * Returns the number of JVBs known to this bridge selector. Not all of them
     * have to be operational.
     */
    public int getKnownBridgesCount()
    {
        return bridges.size();
    }

    /**
     * Lists all operational JVB instance JIDs currently known to this
     * <tt>BridgeSelector</tt> instance.
     *
     * @return a <tt>List</tt> of <tt>String</tt> with bridges JIDs.
     */
    synchronized public List<String> listActiveJVBs()
    {
        ArrayList<String> listing = new ArrayList<>(bridges.size());
        for (BridgeState bridge : bridges.values())
        {
            if (bridge.isOperational())
            {
                listing.add(bridge.jid);
            }
        }
        return listing;
    }

    private void notifyBridgeUp(BridgeState bridge)
    {
        logger.debug("Propagating new bridge added event: " + bridge.jid);

        eventAdmin.sendEvent(
            BridgeEvent.createBridgeUp(bridge.jid));
    }

    private void notifyBridgeDown(BridgeState bridge)
    {
        logger.debug("Propagating bridge went down event: " + bridge.jid);

        eventAdmin.sendEvent(
            BridgeEvent.createBridgeDown(bridge.jid));
    }

    /**
     * Initializes this instance by loading the config and obtaining required
     * service references.
     */
    public void init()
    {
        ConfigurationService config = FocusBundleActivator.getConfigService();

        String mappingPropertyValue = config.getString(BRIDGE_TO_PUBSUB_PNAME);

        if (!StringUtils.isNullOrEmpty(mappingPropertyValue))
        {
            String[] pairs = mappingPropertyValue.split(";");
            for (String pair : pairs)
            {
                String[] bridgeAndNode = pair.split(":");
                if (bridgeAndNode.length != 2)
                {
                    logger.error("Invalid mapping element: " + pair);
                    continue;
                }

                String bridge = bridgeAndNode[0];
                String pubSubNode = bridgeAndNode[1];
                pubSubToBridge.put(pubSubNode, bridge);

                logger.info("Pub-sub mapping: " + pubSubNode + " -> " + bridge);
            }
        }

        setFailureResetThreshold(
                config.getLong(
                        BRIDGE_FAILURE_RESET_THRESHOLD_PNAME,
                        DEFAULT_FAILURE_RESET_THRESHOLD));

        logger.info(
            "Bridge failure reset threshold: " + getFailureResetThreshold());

        this.eventAdmin = FocusBundleActivator.getEventAdmin();
        if (eventAdmin == null)
        {
            throw new IllegalStateException("EventAdmin service not found");
        }
    }

    /**
     * Finds the version of the videobridge identified by given
     * <tt>bridgeJid</tt>.
     *
     * @param bridgeJid the XMPP address of the videobridge for which we want to
     *        obtain the version.
     *
     * @return {@link Version} instance which holds the details about JVB
     *         version or <tt>null</tt> if unknown.
     */
    public Version getBridgeVersion(String bridgeJid)
    {
        BridgeState bridgeState = bridges.get(bridgeJid);

        return bridgeState != null ? bridgeState.version : null;
    }

    /**
     * Served as a thrift server, after spawning new node, this method can return
     * the corresponding jvb number, to start the jvb accordingly.
     * @return  Jvb number for new node
     * @throws TException
     */
    @Override
    public synchronized String getJvbNumber() throws TException {
        int num = bridges.size() + 1;
        logger.info("bridge size:" + bridges.size());
        if (jvbList.contains(num)) {
            num++;
        }
        String jvb = Integer.toString(num);
        // change 1 to 01, and all under 10
        if (num < 10) {
            jvb =  "0" + Integer.toString(num);
        }

        jvbList.add(num);

        return jvb;
    }

    /**
     * Start apache thrift server
     */
    @Override
    public void run() {
        try {
            TServerTransport serverTransport = new TServerSocket(THRIFT_SERVER_PORT);
//            TServer server = new TSimpleServer(new TServer.Args(serverTransport).processor(processor));
            Scheduler.Processor processor = new Scheduler.Processor(this);
            // Use this for a multithreaded server
             TServer server = new TThreadPoolServer(new TThreadPoolServer.Args(serverTransport).processor(processor));

            System.out.println("Starting the simple server...");
            server.serve();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Class holds videobridge state and implements {@link java.lang.Comparable}
     * interface to find least loaded bridge.
     */
    public class BridgeState
        implements Comparable<BridgeState>
    {
        /**
         *
         */
        private final InfluxDbHealthChecker checker = new InfluxDbHealthChecker();

        /**
         * Videobridge XMPP address.
         */
        private final String jid;

        /**
         * If not set we consider it highly occupied,
         * because no stats we have been fetched so far.
         */
        private double conferenceCount = 0;

        /**
         * If not set we consider it highly occupied,
         * because no stats we have been fetched so far.
         */
        private double videoChannelCount = 0;

        /**
         * If not set we consider it highly occupied,
         * because no stats we have been fetched so far.
         */
        private double videoStreamCount = 0;

        private double participants = 0;

        /**
         * Holds bridge version(if known - not all bridge version are capable of
         * reporting it).
         */
        private final Version version;

        /**
         * Stores *operational* status which means it has been successfully used
         * by the focus to allocate the channels. It is reset to false when
         * focus fails to allocate channels, but it gets another chance when all
         * currently working bridges go down and might eventually get elevated
         * back to *operational* state.
         */
        private boolean isOperational = true /* we assume it is operational */;

        /**
         * The time when this instance has failed.
         */
        private long failureTimestamp = 0;

        private double cpuUsage = 0;
        private double memoryUsage = 0;
        private double lossRateUpload = 0;
        private double lossRateDownload = 0;
        private double uploadRate = 0;
        private double downloadRate = 0;
        private double aggregatedJitter = 0;
        private double rtt = 0;

        // STRINGS
        public static final String CONFERENCE_COUNT = "conferences";
        public static final String VIDEOCHANNEL_COUNT = "videochannels";
        public static final String VIDEOSTREAM_COUNT = "videostreams";
        public static final String PARTICIPANTS = "participants";
        public static final String CPU_USAGE = "cpu_usage";
        public static final String MEMORY_USSED = "used_memory";
        public static final String MEMORY_TOTAL = "total_memory";
        public static final String LOSS_UPLOAD= "loss_rate_upload";
        public static final String LOSS_DOWNLOADLOAD= "loss_rate_download";
        public static final String DOWNLOAD_RATE = "bit_download_rate";
        public static final String UPLOAD_RATE = "bit_upload_rate";
        public static final String AGGREGATED_JITTER = "jitter_aggregate";

//        private final HashMap<String, > metricsMap = new HashMap();

        public static final String ENABLE_PREDICTION = "ENABLE_PREDICTION";
        // THRESHOLDS
        public static final double CPU_THRESHOLD = 0.85;
        public static final double MEMORY_USAGE_THRESHOLD = 0.80;
        public static final double JITTER_THRESHOLD = 10.0;
        public static final double RTT_THRESHOLD = 10.0;
        public static final double LOSS_UPLOAD_THRESHOLD = 0.01;
        public static final double LOSS_DOWNLOAD_THRESHOLD = 0.01;

        public final String[] metricList = {"audiochannels" // 0
                , "avgload_1" // 1
                , "avgload_5" // 2
                , "avgload_15" // 3
                , "bit_rate_download" // 4
                , "bit_rate_upload" // 5
                , "conferences"  // 6
                , "cpu_idle"  // 7
                , "cpu_iowait"  // 8
                , "cpu_system"  // 9
                , "cpu_usage"   // 10
                , "cpu_user"    // 11
                , "jitter_aggregate"   // 12
                , "largest_conference"  // 13
                , "loss_rate_download"  // 14
                , "loss_rate_upload"    // 15
                , "packet_rate_download"   // 16
                , "packet_rate_upload"     // 17
                , "participants"   // 18
                , "rtt_aggregate"  // 19
                , "threads"        // 20
                , "total_channels" // 21
                , "total_conference_seconds"     // 22
                , "total_conferences_completed"  // 23
                , "total_conferences_created"    // 24
                , "total_failed_conferences"     // 25
                , "total_memory"                 // 26
                , "total_no_payload_channels"    // 27
                , "total_no_transport_channels"  // 28
                , "total_partially_failed_conferences"  // 29
                , "total_udp_connections"  // 30
                , "used_memory" // 31
                , "videochannels" // 32
                , "videostreams"  // 33
        };

        public final String[] concernedMetrics = {
            "cpu_usage"
                , "loss_rate_download"
                , "loss_rate_upload"
                , "jitter_aggregate"
                , "rtt_aggregate"
                , "used_memory"
        };

        private final String predictionDataPath;
        private String[] trainingMetadata;
        private HashMap<String, Double[]> trainingParameters;

        public double getCpuThreshold() {
            String env = System.getenv("CPU_THRESHOLD");
            if (env != null) {
                return Double.valueOf(env);
            } else {
                return CPU_THRESHOLD;
            }
        }

        public double getMemoryUsageThreshold() {
            String env = System.getenv("MEMORY_USAGE_THRESHOLD");
            if (env != null) {
                return Double.valueOf(env);
            } else {
                return MEMORY_USAGE_THRESHOLD;
            }
        }

        public double getJitterThreshold() {
            String env = System.getenv("JITTER_THRESHOLD");
            if (env != null) {
                return Double.valueOf(env);
            } else {
                return JITTER_THRESHOLD;
            }
        }

        public double getRttThreshold() {
            String env = System.getenv("RTT_THRESHOLD");
            if (env != null) {
                return Double.valueOf(env);
            } else {
                return RTT_THRESHOLD;
            }
        }

        public double getLossUploadThreshold() {
            String env = System.getenv("LOSS_UPLOAD_THRESHOLD");
            if (env != null) {
                return Double.valueOf(env);
            } else {
                return LOSS_UPLOAD_THRESHOLD;
            }
        }

        public double getLossDownloadThreshold() {
            String env = System.getenv("LOSS_DOWNLOAD_THRESHOLD");
            if (env != null) {
                return Double.valueOf(env);
            } else {
                return LOSS_DOWNLOAD_THRESHOLD;
            }
        }


        BridgeState(String bridgeJid, Version version)
        {
            Assert.notNullNorEmpty(bridgeJid, "bridgeJid: " + bridgeJid);

            this.jid = bridgeJid;
            this.version = version;
            this.predictionDataPath = System.getenv(ENABLE_PREDICTION);
        }

        public void setConferenceCount(int conferenceCount)
        {
            if (this.conferenceCount != conferenceCount)
            {
                logger.info(
                    "Conference count for: " + jid + ": " + conferenceCount);
            }
            this.conferenceCount = conferenceCount;
        }

        public double getConferenceCount()
        {
            return this.conferenceCount;
        }

        /**
         * Return the number of channels used.
         * @return the number of channels used.
         */
        public double getVideoChannelCount()
        {
            return this.videoChannelCount;
        }

        public double updateAndGetConferenceCount()
        {
            this.conferenceCount = checker.getConferences(jid);
            return this.conferenceCount;
        }

        /**
         * Return the number of channels used.
         * @return the number of channels used.
         */
        public double updateAndGetVideoChannelCount()
        {
            this.videoChannelCount = checker.getVideoChannels(jid);
            return this.videoChannelCount;
        }

        /**
         * Sets the number of channels used.
         * @param channelCount the number of channels used.
         */
        public void setVideoChannelCount(double channelCount)
        {
            this.videoChannelCount = channelCount;
        }

        /**
         * Returns the number of streams used.
         * @return the number of streams used.
         */
        public double getVideoStreamCount()
        {
            this.videoStreamCount = checker.getVideoStreams(jid);
            return this.videoStreamCount;
        }

        /**
         *
         * @return
         */
        public double getConferences() {
            return this.conferenceCount;
        }

        /**
         *
         * @return
         */
        public double updateAndGetParticipants() {
            this.participants = checker.getParticipants(jid);
            return this.participants;
        }

        public double updateAndGetCPUUsage() {
            this.cpuUsage =  checker.getCPUUsage(jid);
            return this.cpuUsage;
        }

        public double updateAndGetMemoryUsage() {
            this.memoryUsage =  checker.getMemoryUsage(jid);
            return this.memoryUsage;
        }

        public double updateAndGetLossRateUpload() {
            this.lossRateUpload = checker.getLossRateUpload(jid);
            return this.lossRateUpload;
        }

        public double updateAndGetLossRateDownload() {
            this.lossRateDownload = checker.getLossRateDownload(jid);
            return this.lossRateDownload;
        }

        public double updateAndGetUploadRate() {
            this.uploadRate = checker.getUploadRate(jid);
            return this.uploadRate;
        }

        public double updateAndGetDownloadRate() {
            this.downloadRate = checker.getDownloadRate(jid);
            return this.downloadRate;
        }

        public double updateAndGetAggregatedJitter() {
            this.aggregatedJitter = checker.getAggregatedJitter(jid);
            return this.aggregatedJitter;
        }

        /**
         * Returns the number of streams used.
         * @return the number of streams used.
         */
        public double updateAndGetVideoStreamCount()
        {
            this.videoStreamCount = checker.getVideoStreams(jid);
            return this.videoStreamCount;
        }

        /**
         *
         * @return
         */
        public double updateAndGetConferences() {
            this.conferenceCount = checker.getConferences(jid);
            return this.conferenceCount;
        }

        public double updateAndGetRTT() {
            this.rtt = checker.getRTT(jid);
            return this.rtt;
        }

        /**
         *
         * @return
         */
        public double getParticipants() {
            return this.participants;
        }

        public double getCPUUsage() {
            return this.cpuUsage;
        }

        public double getMemoryUsage() {
            return this.memoryUsage;
        }

        public double getLossRateUpload() {
            return this.lossRateUpload;
        }

        public double getLossRateDownloadload() {
            return this.lossRateDownload;
        }

        public double getUploadRate() {
            return this.uploadRate;
        }

        public double getDownloadRate() {
            return this.downloadRate;
        }

        public double getAggregatedJitter() {
            return this.aggregatedJitter;
        }

        public double getRTT() {
            return this.rtt;
        }

        /**
         * Sets the stream count currently used.
         * @param streamCount the stream count currently used.
         */
        public void setVideoStreamCount(double streamCount)
        {
            if (this.videoStreamCount != streamCount)
            {
                logger.info(
                    "Video stream count for: " + jid + ": " + streamCount);
            }
            this.videoStreamCount = streamCount;
        }

        public void setIsOperational(boolean isOperational)
        {
            this.isOperational = isOperational;

            if (!isOperational)
            {
                // Remember when the bridge has failed
                failureTimestamp = System.currentTimeMillis();
            }
        }

        public boolean isOperational()
        {
            // Check if we should give this bridge another try
            verifyFailureThreshold();

            return isOperational;
        }

        /**
         * Verifies if it has been long enough since last bridge failure to give
         * it another try(reset isOperational flag).
         */
        private void verifyFailureThreshold()
        {
            if (isOperational)
            {
                return;
            }

            if (System.currentTimeMillis() - failureTimestamp
                    > getFailureResetThreshold())
            {
                logger.info("Resetting operational status for " + jid);
                isOperational = true;
            }
        }

        /**
         * The least value is returned the least the bridge is loaded.
         * TODO more sophisticated thresholds mechanism
         * {@inheritDoc}
         */
        @Override
        public int compareTo(BridgeState o)
        {
            boolean meOperational = isOperational();
            boolean otherOperational = o.isOperational();

            if (meOperational && !otherOperational)
                return -1;
            else if (!meOperational && otherOperational)
                return 1;

            return (int) (updateAndGetVideoStreamCount() - o.updateAndGetVideoStreamCount());
        }

        public HashMap<String, Double[]> getTrainingParameters() {
            if (trainingParameters != null) {
                return trainingParameters;
            }

            JSONParser parser = new JSONParser();
            trainingParameters = new HashMap<String, Double[]>();
            try {
                JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(predictionDataPath));
                JSONArray metaJsonArray = (JSONArray) jsonObject.get("metadata");
                trainingMetadata = new String[metaJsonArray.size()];
                Iterator<String> metaIter = metaJsonArray.iterator();

                for (int i = 0; i < metaJsonArray.size(); i++) {
                    trainingMetadata[i] = (String) metaJsonArray.get(i);
                }

                jsonObject.remove("metadata");
                Iterator iterator = jsonObject.entrySet().iterator();

                while (iterator.hasNext()) {
                    Map.Entry entry = (Map.Entry) iterator.next();
                    JSONArray paramJsonArray = (JSONArray) entry.getValue();
                    Double[] paramData = new Double[paramJsonArray.size()];

                    for (int i = 0; i < paramJsonArray.size(); i++) {
                        paramData[i] = (Double) paramJsonArray.get(i);
                    }

                    trainingParameters.put((String) entry.getKey(), paramData);
                }
            } catch (ParseException e) {
                logger.error(e);
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                logger.error(e);
            } catch (IOException e) {
                logger.error(e);
            }

            return trainingParameters;
        }

        public String[] getTrainingMetadata() {
            if (trainingMetadata != null) {
                return trainingMetadata;
            }

            JSONParser parser = new JSONParser();

            try {
                JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(predictionDataPath));
                JSONArray metaJsonArray = (JSONArray) jsonObject.get("metadata");

                trainingMetadata = new String[metaJsonArray.size()];
                Iterator<String> metaIter = metaJsonArray.iterator();

                for (int i = 0; i < metaJsonArray.size(); i++) {
                    trainingMetadata[i] = (String) metaJsonArray.get(i);
                }

            } catch (ParseException e) {
                logger.error(e);
            } catch (FileNotFoundException e) {
                logger.error(e);
            } catch (IOException e) {
                logger.error(e);
            }

            return trainingMetadata;
        }

        private double predict(String metric, int conferenceSize) {
            Double[] params = getTrainingParameters().get(metric);
            double predicted = 0.0;

            for (int i = 0; i < getTrainingMetadata().length-2; i++) {
                predicted += checker.getDoubleMetric(jid, getTrainingMetadata()[i]) * params[i];
            }
            predicted += (double) conferenceSize * params[params.length-2];
            predicted += params[params.length-1];
            return predicted;
        }

        /**
         * Check if the given jvb reaches the preset thresholds
         * @return  true if it exceeds thresholds
         */
        public boolean exceedsThresholds(int conferenceSize) {
            logger.info("== Checking thresholds...");

            if (predictionDataPath != null) {
                logger.info("Prediction ENABLED.");

                // cpu_usage
                double cpuPredicted = predict("cpu_usage", conferenceSize);
                logger.info("cpu_usage Predicted:" + cpuPredicted + "  -- Current cpu_usage:" + checker.getDoubleMetric(jid, "cpu_usage"));

                // jitter_aggregate
                double jitterPredicted = predict("jitter_aggregate", conferenceSize);
                logger.info("jitter_aggregate Predicted:" + jitterPredicted + " -- Current jitter_aggregate:" + checker.getDoubleMetric(jid, "jitter_aggregate"));

                // used_memory
                double memoryPredicted = predict("used_memory", conferenceSize);
                double totalMemory = checker.getDoubleMetric(jid, "total_memory");
                logger.info("used_memory Predicted:" + memoryPredicted/totalMemory + " -- Current used_memory:" + checker.getDoubleMetric(jid, "used_memory"));

                // loss_rate_upload
                double lossUpPredicted = predict("loss_rate_upload", conferenceSize);
                logger.info("loss_rate_upload Predicted:" + lossUpPredicted + " -- Current loss_rate_upload:" + checker.getDoubleMetric(jid, "loss_rate_upload"));

                // rtt_aggregate
                double rttPredicted = predict("rtt_aggregate", conferenceSize);
                logger.info("rtt_aggregate Predicted:" + rttPredicted + " -- Current rtt_aggregate:" + checker.getDoubleMetric(jid, "rtt_aggregate"));

                logger.info("Current cpu:" + checker.getDoubleMetric(jid, "cpu_usage"));
                logger.info("Current jitter:" + checker.getDoubleMetric(jid, "jitter_aggregate"));
                logger.info("Current mem:" + checker.getDoubleMetric(jid, "used_memory")/checker.getDoubleMetric(jid, "total_memory"));
                logger.info("Current loss:" + checker.getDoubleMetric(jid, "loss_rate_upload"));
                logger.info("Current rtt:" + checker.getDoubleMetric(jid, "rtt_aggregate"));

                if (cpuPredicted > getCpuThreshold() * getScalingFactor() // !
                        || memoryPredicted/totalMemory > getMemoryUsageThreshold() * getScalingFactor()
                        || lossUpPredicted > getLossUploadThreshold() * getScalingFactor()
                        || jitterPredicted > getJitterThreshold() * getScalingFactor() // !
                        || rttPredicted > getRttThreshold() * getScalingFactor()             //                              |
                        || checker.getDoubleMetric(jid, "cpu_usage") > getCpuThreshold() * getScalingFactor()   // prediction could be smaller \|/
                        || checker.getDoubleMetric(jid, "jitter_aggregate") > getJitterThreshold() * getScalingFactor()
                        || checker.getDoubleMetric(jid, "used_memory")/checker.getDoubleMetric(jid, "total_memory") > getMemoryUsageThreshold() * getScalingFactor()
                        || checker.getDoubleMetric(jid, "loss_rate_upload") > getLossUploadThreshold() * getScalingFactor()
                        || checker.getDoubleMetric(jid, "rtt_aggregate") > getRttThreshold() * getScalingFactor() ) {
                    logger.info("Exceeds thresholds.");
                    return true;
                } else {
                    return false;
                }
            } else { // when ENABLE_PREDICTION is false
                logger.info("Prediction DISABLED.");
                if (checker.getDoubleMetric(jid, "cpu_usage") > getCpuThreshold() * getScalingFactor()   // prediction could be smaller \|/
                        || checker.getDoubleMetric(jid, "jitter_aggregate") > getJitterThreshold() * getScalingFactor()
                        || checker.getDoubleMetric(jid, "used_memory")/checker.getDoubleMetric(jid, "total_memory") > getMemoryUsageThreshold() * getScalingFactor()
                        || checker.getDoubleMetric(jid, "loss_rate_upload") > getLossUploadThreshold() * getScalingFactor()
                        || checker.getDoubleMetric(jid, "rtt_aggregate") > getRttThreshold() * getScalingFactor() ) {
                    logger.info("Exceeds thresholds.");
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
}
