package org.jitsi.jicofo.learning;

import java.io.*;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by mluo on 8/16/16.
 */
public class ModelMap {
    private final ConcurrentHashMap<String, HashMap<String, Double>> modelMap = new ConcurrentHashMap();
    private static ModelMap ourInstance = new ModelMap();

    public static ModelMap getInstance() {
        return ourInstance;
    }

    private ModelMap() {
    }

    public ConcurrentHashMap<String, HashMap<String, Double>> getModelMap() {
        return modelMap;
    }

    public void writeToFile(File file) throws IOException {
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(modelMap);
        oos.close();
    }

    public void readFromFile(File file) throws IOException, ClassNotFoundException {
        if (!file.exists()) {
            return;
        }
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        ConcurrentHashMap<String, HashMap<String, Double>> fileObj = (ConcurrentHashMap<String, HashMap<String, Double>>) ois.readObject();
        ois.close();
    }
}
