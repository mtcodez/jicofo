package org.jitsi.jicofo.learning;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;

/**
 * Created by Mante Luo on 8/14/16.
 */
public class Learner {
    private final BridgeLearner.Client client;
    private static final String HOST = "localhost";
    private static final int PORT = 9090;

    public Learner() throws TTransportException {
        TTransport transport = new TSocket(HOST, PORT);
        transport.open();
        this.client = new BridgeLearner.Client(new TBinaryProtocol(transport));
    }

    public TrainedModel train(String jvb) throws TException {
        return client.train(jvb);
    }
}
