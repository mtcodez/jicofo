package org.jitsi.jicofo;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;

/**
 * Created by Mante Luo on 8/9/16.
 */
public class InfluxDbHealthChecker {
    private static final String DEFAULT_HOST = "http://localhost:8086";
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin";
    private static final String DATABASE_NAME = "metrics";
    private static final String DEFAULT_INTERVAL = "30s";
    private static final String DEFAULT_AVERAGE_TIME = "2s";
    private static final String DEFAULT_TIME_UPPERBOUND = "1s";
    private static final String FORMAT_STRING = "SELECT value FROM %s WHERE time >= now() - %s AND jvb = '%s' limit 1";
    private static final String INFLUXDB_HOST = "INFLUXDB_HOST";
    private static final String INFLUXDB_INTERVAL = "INFLUXDB_INTERVAL";
    private static final String INFLUXDB_AVERAGE_TIME = "INFLUXDB_AVERAGE_TIME";
    private static final String INFLUXDB_TIME_UPPERBOUND = "INFLUXDB_TIME_UPPERBOUND";


    private final InfluxDB influxDB;

    public InfluxDbHealthChecker() {
        influxDB = InfluxDBFactory.connect(getHost(), USERNAME, PASSWORD);
    }

    private String getHost() {
        String host = System.getenv(INFLUXDB_HOST);
        if (host == null) {
            return DEFAULT_HOST;
        } else {
            return host;
        }
    }

    private String getInterval() {
        String interval = System.getenv(INFLUXDB_INTERVAL);
        if (interval == null) {
            return DEFAULT_INTERVAL;
        } else {
            return interval;
        }
    }

    private String getAverageTime() {
        String averageTime = System.getenv(INFLUXDB_AVERAGE_TIME);
        if (averageTime == null) {
            return DEFAULT_AVERAGE_TIME;
        } else {
            return averageTime;
        }
    }

    private String getTimeUpperBound() {
        String upperBound = System.getenv(INFLUXDB_TIME_UPPERBOUND);
        if (upperBound == null) {
            return DEFAULT_TIME_UPPERBOUND;
        } else {
            return upperBound;
        }
    }

    public double getCPUUsage(String jvb) {
        Query query = new Query(String.format(FORMAT_STRING, "cpu_usage", getInterval(), jvb, getAverageTime()), DATABASE_NAME);
        QueryResult result = influxDB.query(query);
        return getResult(result);
    }

    public double getMemoryUsage(String jvb) {
        Query usedQuery = new Query(String.format(FORMAT_STRING, "used_memory", getInterval(), jvb, getAverageTime()), DATABASE_NAME);

        double used = getResult(influxDB.query(usedQuery));
        Query totalQuery = new Query(String.format(FORMAT_STRING, "total_memory", getInterval(), jvb, getAverageTime()), DATABASE_NAME);

        double total = getResult(influxDB.query(totalQuery));

        return used / total;
    }

    public double getLossRateUpload(String jvb) {
        Query query = new Query(String.format(FORMAT_STRING, "loss_rate_upload", getInterval(), jvb, getAverageTime()), DATABASE_NAME);
        return getResult(influxDB.query(query));
    }

    public double getLossRateDownload(String jvb) {
        Query query = new Query(String.format(FORMAT_STRING, "loss_rate_download", getInterval(), jvb, getAverageTime()), DATABASE_NAME);
        return getResult(influxDB.query(query));
    }

    public double getUploadRate(String jvb) {
        Query query = new Query(String.format(FORMAT_STRING, "bit_rate_upload", getInterval(), jvb, getAverageTime()), DATABASE_NAME);
        return getResult(influxDB.query(query));
    }

    public double getDownloadRate(String jvb) {
        Query query = new Query(String.format(FORMAT_STRING, "bit_rate_download", getInterval(), jvb, getAverageTime()), DATABASE_NAME);
        return getResult(influxDB.query(query));
    }

    public double getAggregatedJitter(String jvb) {
        Query query = new Query(String.format(FORMAT_STRING, "jitter_aggregate", getInterval(), jvb, getAverageTime()), DATABASE_NAME);
        return getResult(influxDB.query(query));
    }

    public double getVideoStreams(String jvb) {
        Query query = new Query(String.format(FORMAT_STRING, "videostreams", getInterval(), jvb, getAverageTime()), DATABASE_NAME);
        return getResult(influxDB.query(query));
    }

    public double getAudioChannels(String jvb) {
        Query query = new Query(String.format(FORMAT_STRING, "audiochannels", getInterval(), jvb, getAverageTime()), DATABASE_NAME);
        return getResult(influxDB.query(query));
    }

    public double getVideoChannels(String jvb) {
        return getAudioChannels(jvb);
    }

    public double getParticipants(String jvb) {
        Query query = new Query(String.format(FORMAT_STRING, "participants", getInterval(), jvb, getAverageTime()), DATABASE_NAME);
        return getResult(influxDB.query(query));
    }

    public double getConferences(String jvb) {
        Query query = new Query(String.format(FORMAT_STRING, "conferences", getInterval(), jvb, getAverageTime()), DATABASE_NAME);
        return getResult(influxDB.query(query));
    }

    public double getRTT(String jvb) {
        Query query = new Query(String.format(FORMAT_STRING, "rtt_aggregate", getInterval(), jvb, getAverageTime()), DATABASE_NAME);
        return getResult(influxDB.query(query));
    }

    public int getIntMetric(String jvb, String metric) {
        Query query = new Query(String.format(FORMAT_STRING, metric, getInterval(), jvb, getAverageTime()), DATABASE_NAME);
        return getResultInt(influxDB.query(query));
    }

    public double getDoubleMetric(String jvb, String metric) {
        Query query = new Query(String.format(FORMAT_STRING, metric, getInterval(), jvb, getAverageTime()), DATABASE_NAME);
        return getResult(influxDB.query(query));
    }

    private double getResult(QueryResult result) {
        return (double) result.getResults().get(0).getSeries().get(0).getValues().get(0).get(1);
    }

    private int getResultInt(QueryResult result) {
        return (int) result.getResults().get(0).getSeries().get(0).getValues().get(0).get(1);
    }
}
