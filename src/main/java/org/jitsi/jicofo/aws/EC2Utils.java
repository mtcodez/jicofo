package org.jitsi.jicofo.aws;

import org.apache.commons.io.IOUtils;
import org.jitsi.jicofo.FocusBundleActivator;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/**
 * Created by mluo on 8/23/16.
 */
public class EC2Utils {
    public static final String ENDPOINT = "https://ec2.us-west-2.amazonaws.com";

    public static void startInstance(String ami, String key, String securityGroup, String instanceType) {
        try {
            getProcessResult(new String[]{"/bin/sh", "-c",
                    "aws ec2 run-instances --image-id " + ami + " --security-group-ids " + securityGroup + " --count 1 --instance-type " + instanceType + " --key-name " + key + " --query 'Instances[0].InstanceId'"});
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Future<String> getProcessResult(final String[] command) throws IOException {
        return FocusBundleActivator.getSharedThreadPool().submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                final Process process = Runtime.getRuntime().exec(command);
                String output = null;
                try (InputStream input = process.getInputStream()) {
                    output = IOUtils.toString(input, StandardCharsets.UTF_8);
                    process.waitFor();
                }
                return output;
            }
        });
    }
}
