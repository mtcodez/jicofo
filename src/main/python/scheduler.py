import sys
import glob

from scheduling import Scheduler
from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol

if __name__ == '__main__':
    transport = TSocket.TSocket('localhost', 10090)

    # Buffering is critical. Raw sockets are very slow
    transport = TTransport.TBufferedTransport(transport)

    # Wrap in a protocol
    protocol = TBinaryProtocol.TBinaryProtocol(transport)

    # Create a client to use the protocol encoder
    client = Scheduler.Client(protocol)

    # Connect!
    transport.open()

    print('getJvbNumber():', client.getJvbNumber())