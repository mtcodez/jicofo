package org.jitsi.jicofo.learning;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Mante Luo on 8/14/16.
 */
public class LearnerIT {
    @Test
    public void train() throws Exception {
        Learner learner = new Learner();
        System.out.println(learner.train("jitsi-videobridge1.ip-172-31-36-172.us-west-2.compute.internal"));
    }

}