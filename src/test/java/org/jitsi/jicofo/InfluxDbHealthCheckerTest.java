package org.jitsi.jicofo;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Mante Luo on 8/9/16.
 */
public class InfluxDbHealthCheckerTest {
    @Test
    public void getCPUUsage() throws Exception {
        InfluxDbHealthChecker checker = new InfluxDbHealthChecker();
        System.out.println(checker.getCPUUsage("jitsi-videobridge1"));
    }

    @Test
    public void getMemoryUsage() throws Exception {
        InfluxDbHealthChecker checker = new InfluxDbHealthChecker();
        System.out.println(checker.getMemoryUsage("jitsi-videobridge1"));
    }

//    @Test
//    public void getRTPLoss() throws Exception {
//        InfluxDbHealthChecker checker = new InfluxDbHealthChecker();
//        System.out.println(checker.getRTPLoss("jitsi-videobridge1"));
//    }

    @Test
    public void getUploadRate() throws Exception {
        InfluxDbHealthChecker checker = new InfluxDbHealthChecker();
        System.out.println(checker.getUploadRate("jitsi-videobridge1"));
    }

//    @Test
//    public void getUploadJitter() throws Exception {
//        InfluxDbHealthChecker checker = new InfluxDbHealthChecker();
//        System.out.println(checker.getUploadJitter("jitsi-videobridge1"));
//    }
//
//    @Test
//    public void getDownloadJitter() throws Exception {
//        InfluxDbHealthChecker checker = new InfluxDbHealthChecker();
//        System.out.println(checker.getDownloadJitter("jitsi-videobridge1"));
//    }

    @Test
    public void testStringFormat() throws Exception {
        System.out.println(String.format("SELECT mean(value) FROM %s WHERE time >= now() - %s  AND time < now() - %s AND jvb = '%s' GROUP BY time(%s) limit 1", "cpu_usage", "40s", "1s", "jvb01", "2s"));
    }

    @Test
    public void getVideoStreams() throws Exception {
        InfluxDbHealthChecker checker = new InfluxDbHealthChecker();
        System.out.println(checker.getVideoStreams("jitsi-videobridge1"));
    }
    @Test
    public void getAudioChannels() throws Exception {
        InfluxDbHealthChecker checker = new InfluxDbHealthChecker();
        System.out.println(checker.getAudioChannels("jitsi-videobridge1"));
    }
    @Test
    public void getParticipants() throws Exception {
        InfluxDbHealthChecker checker = new InfluxDbHealthChecker();
        System.out.println(checker.getParticipants("jitsi-videobridge1"));
    }
    @Test
    public void getConferences() throws Exception {
        InfluxDbHealthChecker checker = new InfluxDbHealthChecker();
        System.out.println(checker.getConferences("jitsi-videobridge1"));
    }

}