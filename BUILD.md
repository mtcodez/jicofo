It fails if simply type ant disk.{os-type}
Rather, you should do the following:
1) Download maven ant tasks from http://central.maven.org/maven2/org/apache/maven/maven-ant-tasks/2.1.3/maven-ant-tasks-2.1.3.jar, and place it under jicofo/lib
2) Add the following to build.xml
<project name="foo" default="foo" xmlns:artifact="antlib:org.apache.maven.artifact.ant">

  <path id="maven-ant-tasks.classpath" path="lib/maven-ant-tasks-2.1.3.jar" />
  <typedef resource="org/apache/maven/artifact/ant/antlib.xml"
       uri="antlib:org.apache.maven.artifact.ant"
       classpathref="maven-ant-tasks.classpath" />
3) replace the file under ~/.m2/repository/org/sonatype/oss/oss-parent/3
with https://repo1.maven.org/maven2/org/sonatype/oss/oss-parent/3/oss-parent-3.pom

Well, it will all work now.
Happy hacking!
